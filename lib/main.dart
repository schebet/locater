import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'providers/place_details_provider.dart';
import 'providers/prediction_provider.dart';
import 'package:find/screens/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => PredictionProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => PlaceDetailsProvider(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomePage(),
      ),
    );
  }
}
