import 'package:flutter/material.dart';
import 'package:find/models/auto_complete.dart';
import 'package:find/services/http_service.dart';

class PredictionProvider with ChangeNotifier {
  List<Predictions> _predictions = [];
  bool _loading = false;

  List<Predictions> get predictions => _predictions;

  bool get loading => _loading;

  Future<void> getPrediction(String name) async {
    _loading = true;
    notifyListeners();
    try {
      _predictions = (await loadPredictions(name)).predictions;
      _loading = false;
      notifyListeners();
    } catch (e) {
      //handle error
      _loading = false;
      notifyListeners();
    }
  }
}
