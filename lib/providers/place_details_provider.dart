import 'package:flutter/material.dart';
import 'package:find/models/place_details.dart';
import 'package:find/services/http_service.dart';

class PlaceDetailsProvider with ChangeNotifier {
  Place _place;
  bool _loading = false;

  Place get place => _place;

  bool get loading => _loading;

  Future<void> getPlaceDetails(String placeId) async {
    _loading = true;
    notifyListeners();

    try {
      _place = (await loadPlaceDetails(placeId)).place;
      _loading = false;
      notifyListeners();
    } catch (e) {
      //handle error
      _loading = false;
      notifyListeners();
    }
  }
}
