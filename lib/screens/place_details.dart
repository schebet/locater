import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:find/providers/place_details_provider.dart';

class PlaceDetails extends StatefulWidget {
  final String placeId;

  const PlaceDetails({Key key, this.placeId}) : super(key: key);

  @override
  _PlaceDetailsState createState() => _PlaceDetailsState();
}

class _PlaceDetailsState extends State<PlaceDetails> {
  @override
  void initState() {
    super.initState();

    Future.microtask(() =>
        context.read<PlaceDetailsProvider>().getPlaceDetails(widget.placeId));
  }

  @override
  Widget build(BuildContext context) {
    var state = context.watch<PlaceDetailsProvider>();
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: Text(
          "Place details",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(8.0),
        margin: EdgeInsets.all(8.0),
        child: state.loading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Name : ${state.place.name}"),
                  SizedBox(
                    height: 8.0,
                  ),
                  Text("Latitude :${state.place.geometry.location.lat}"),
                  SizedBox(
                    height: 8.0,
                  ),
                  Text("Longitude : ${state.place.geometry.location.lng}")
                ],
              ),
      ),
    );
  }
}
