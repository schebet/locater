import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:find/providers/prediction_provider.dart';

import 'place_details.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var state = context.watch<PredictionProvider>();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Locater",
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(8.0),
          margin: EdgeInsets.all(8.0),
          child: Column(
            children: [
              Search(),
              state.loading
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : ListView.builder(
                      itemCount: state.predictions.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PlaceDetails(
                                          placeId:
                                              state.predictions[index].placeId,
                                        )));
                          },
                          child: Card(
                            elevation: 6.0,
                            child: Container(
                              padding: EdgeInsets.all(8.0),
                              margin: EdgeInsets.all(8.0),
                              child: Text(
                                  "${state.predictions[index].description}"),
                            ),
                          ),
                        );
                      })
            ],
          ),
        ),
      ),
    );
  }
}

class Search extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 6.0,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        child: TextField(
          style: TextStyle(
            fontSize: 15.0,
            color: Colors.black,
          ),
          decoration: InputDecoration(
            // contentPadding: EdgeInsets.all(10.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: BorderSide(
                color: Colors.white,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(10.0),
            ),
            hintText: "Search",
            hintStyle: TextStyle(
              fontSize: 15.0,
              color: Colors.black,
            ),
          ),
          maxLines: 1,
          onChanged: (value) {
            context.read<PredictionProvider>().getPrediction(value);
          },
        ),
      ),
    );
  }
}
