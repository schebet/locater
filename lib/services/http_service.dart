import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;
import 'package:find/models/auto_complete.dart';
import 'package:find/models/place_details.dart';

Future<AutoComplete> loadPredictions(String name) async {
  try {
    var _url =
        "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=1600+$name&key=AIzaSyBE_qgiZEQlaDJgy55-lksIA_29OVklZ4w&sessiontoken=1234567890&components=country:ke";

    var _res = await http.get(Uri.parse(_url));

    return AutoComplete.fromJson(json.decode(_res.body));
  } catch (e) {
    throw 'Error';
  }
}

Future<PlaceDetails> loadPlaceDetails(String placeId) async {
  try {
    var _url =
        "https://maps.googleapis.com/maps/api/place/details/json?placeid=$placeId&key=AIzaSyBE_qgiZEQlaDJgy55-lksIA_29OVklZ4w";
    log(_url);
    var _res = await http.get(Uri.parse(_url));


    return PlaceDetails.fromJson(json.decode(_res.body));
  } catch (err) {
    throw 'Error:$err';
  }
}
