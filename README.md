# find

 A flutter application that uses the Google API's find places and autocomplete web services.

 ## Screenshots
<p float="left">
  <img src="https://user-images.githubusercontent.com/57434209/116677265-b02cdb00-a9b0-11eb-9e77-1f0ae5e66a10.JPG" width="250" />
    <img src="https://user-images.githubusercontent.com/57434209/116677108-7bb91f00-a9b0-11eb-9dcd-9f6e55681efb.JPG" width="250" />
  <img src="https://user-images.githubusercontent.com/57434209/116677360-caff4f80-a9b0-11eb-81a0-23abdbcec901.JPG" width="250" />
</p>


## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
